import React, { Component } from 'react';
import { ActivityIndicator, Platform, StyleSheet, View, Text, TouchableOpacity, ProgressBarAndroid, ProgressViewIOS } from 'react-native';
import Catalogus from './src/components/catalogus/catalogus'
import NavigationBar from 'react-native-navbar';
import { DbService } from "./src/services/dbService";
import { StateService } from "./src/services/stateService";
import { Icon } from 'react-native-elements';

const titleConfig = {
  title: 'Billiet',
};

type Props = {};
export default class App extends Component<Props> {
  totalAmountOfProducts = null;
  currentAmountOfProducts = null;

  constructor() {
    super();
    this.dbService = new DbService();
    this.stateService = new StateService();
    this.dbService.sync();
    this.state = {loading: true, progress: 0, view: 'listView'};
  }

  componentDidMount() {
    this.dbService.getTotalAmountOfProducts()
      .combineLatest(this.dbService.getCurrentAmountOfProducts())
      .subscribe(([totalAmountOfProducts, currentAmountOfProducts]) => {
        this.setState({loading: false});
        this.totalAmountOfProducts = totalAmountOfProducts;
        this.currentAmountOfProducts = currentAmountOfProducts;
        const progress = this.currentAmountOfProducts / this.totalAmountOfProducts;
        this.setState({progress: progress > 1 ? 1 : progress});
      });
  }

  setView(viewState) {
    this.setState({'view': viewState})
  }

  setDetail(item) {
    this.stateService.setDetail(item)
  }

  render() {
    return (
      <View style={styles.container}>
        <NavigationBar
          style={styles.navigation}
          title={titleConfig}
          leftButton={(<View style={{margin: 10, flexDirection: 'row'}}>
                         <Icon
                            size={38}
                            iconStyle={styles.enabled}
                            onPress={this.setDetail.bind(this, null)}
                            name='chevron-left' />
                         <Icon
                            size={38}
                            iconStyle={[this.state.view === 'listView' ? styles.active : styles.enabled]}
                            onPress={this.setView.bind(this, 'listView')}
                            name='view-list' />
                         <Icon
                            size={34}
                            iconStyle={[this.state.view === 'cardsView' ? styles.active : styles.enabled]}
                            onPress={this.setView.bind(this, 'cardsView')}
                            name='apps' />
                       </View>)}
        />
        {!this.state.loading && this.state.progress == 1 && this.totalAmountOfProducts ?
          <Catalogus stateService={this.stateService} viewState={this.state.view} /> : null
        }
        {!this.state.loading && this.state.progress != 1 && this.totalAmountOfProducts ?
          (
            <View style={{margin: 20}}>
              <Text>Syncing</Text>
              {
                ( Platform.OS === 'android' ) ?
                ( <ProgressBarAndroid styleAttr="Horizontal" progress={ this.state.progress } indeterminate={false} /> ) :
                ( <ProgressViewIOS progress={ this.state.progress } /> )
              }
            </View>
          ) : null}
        {this.state.loading ?
          <View style={{margin: 20}}>
            <ActivityIndicator size="large" />
          </View> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  enabled: {
    color: 'grey',
  },
  active: {
    color: 'black',
  },
  navigation: {
    backgroundColor: 'lightgrey',
    height: 56,
    width: '100%',
    alignSelf: 'stretch'
  },
});

Array.prototype.unique = function() {
  var a = this.concat();
  for(var i=0; i<a.length; ++i) {
    for(var j=i+1; j<a.length; ++j) {
      if(a[i] === a[j])
        a.splice(j--, 1);
    }
  }
  return a;
};

