import React, { Component } from 'react';
import { DbService } from "../../../services/dbService";
import { Text, View, Image, ScrollView } from 'react-native';
import { Card } from 'react-native-elements';

type Props = {};
export default class Detail extends Component<Props> {
  constructor(props) {
    super();
    this.state = {id: props.id};
    this.dbService = new DbService();
    console.log('PROPS: ', props);
  }

  componentDidMount() {
    this.dbService.fetchProductById(this.state.id).then((product) => {
      this.setState({product: product})
    });
    this.dbService.getAttachment(this.state.id, 'product.jpg').then((imgArray) => {
      //TODO: get the actual img: base64?
    });
  }

  render() {
    return (
      <ScrollView>
        {this.state.product ?
        <Card
          title={this.state.product.name}
        >
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{ width: 250, height: 200 }}
              source={{ uri: `${this.props.dbConfig.ip}:${this.props.dbConfig.dbPort}/billiet/1/product.jpg` }}
            />
            <Text>
              €{this.state.product.prices.standard}
            </Text>
          </View>
        </Card> : null}
      </ScrollView>
    )
  }
}