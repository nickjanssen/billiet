import React, { Component } from 'react';
import { DbService } from "../../services/dbService";
import { ListItem, Card } from 'react-native-elements';
import { ActivityIndicator, FlatList, StyleSheet, Text, View, Image, TouchableOpacity, Button } from 'react-native';
import Detail from './detail/detail';

type Props = {};
export default class Catalogus extends Component<Props> {
  amountOfProductsToFetch = 100;
  currentItems = 0;
  products = [];
  config;

  constructor(props) {
    super();
    this.state = {products: null, detailId: null, refreshing: false};
    this.dbService = new DbService();
    this.stateService = props.stateService;
  }

  componentDidMount() {
    this.stateService.getDetail().subscribe((id) => {
      this.setState({detailId: id});
    });
    this.config = this.dbService.getConfig();
    this._loadMore();
  }

  _renderItem = ({item}) => (
    <MyListItem callback={this.setDetail.bind(this)} item={item} dbConfig={this.config} />
  );

  _renderCardsItem = ({item}) => (
    <CardsListItem callback={this.setDetail.bind(this)} item={item} dbConfig={this.config} />
  );

  _getItemLayout = (data, index) => (
    {length: 60, offset: 60 * index, index}
  );

  _keyExtractor = (item, index) => 'list-' + index.toString();

  _loadMore = () => {
    if (!this.state.isFetching) {
      this.setState({isFetching: true});
      this.dbService.fetchProducts(this.currentItems, this.amountOfProductsToFetch).then((result) => {
        this.currentItems += this.amountOfProductsToFetch;
        this.products = this.products.concat(result.rows.map(row => row.doc)).unique();
        this.setState({products: this.products});
        this.setState({isFetching: false});
      });
    }
  };

  setDetail(item) {
    this.stateService.setDetail(item.id);
  };
  
  render() {
    return (
      <View style={{flex: 1}}>

        {this.props.viewState === 'listView' && this.state.detailId === null ?
        <FlatList
          key={'list'}
          extraData={this.state}
          onEndReachedThreshold={80}
          data={this.state.products}
          renderItem={this._renderItem}
          onEndReached={this._loadMore}
          keyExtractor={this._keyExtractor}
          getItemLayout={this._getItemLayout}
          ListFooterComponent={() => {
            return (
              <View style={{margin: 20}}>
                <ActivityIndicator size="large" />
              </View>
            );
          }}
        /> : null}

        {this.props.viewState === 'cardsView' && this.state.detailId === null ?
        <FlatList
          key={'cards'}
          numColumns={3}
          extraData={this.state}
          onEndReachedThreshold={20}
          data={this.state.products}
          onEndReached={this._loadMore}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderCardsItem}
          ListFooterComponent={() => {
            return (
              <View style={{margin: 20}}>
                <ActivityIndicator size="large" />
              </View>
            );
          }}
        /> : null}

        {this.state.detailId !== null ?
          <Detail id={this.state.detailId} dbConfig={this.config}></Detail>
        : null}

      </View>
    );
  }
}

class MyListItem extends React.PureComponent {
  setDetail(item) {
    this.props.callback(item);
  }

  render() {
    return (
      <TouchableOpacity onPress={this.setDetail.bind(this, this.props.item)}>
        <View style={{height: 60}}>
          <ListItem title={
                      <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around'}}>
                        <Text style={{flex: 1, marginLeft: 12}}>{this.props.item.id}</Text>
                        <Text style={{flex: 1}}>{this.props.item.name}</Text>
                        <Text style={{flex: 1}}>€{this.props.item.prices.standard}</Text>
                      </View>
                    }
                    avatar={{uri: `${this.props.dbConfig.ip}:${this.props.dbConfig.dbPort}/billiet/1/product.jpg`}} />
        </View>
      </TouchableOpacity>
    )
  }
}

class CardsListItem extends React.PureComponent {
  setDetail(item) {
    this.props.callback(item);
  }

  render() {
    return (
      <TouchableOpacity style={{width: '33%'}} onPress={this.setDetail.bind(this, this.props.item)}>
        <View>
          <Card
            style={{flex: 1}}
            title={this.props.item.id + ' - ' + this.props.item.name}
            image={{ uri: `${this.props.dbConfig.ip}:${this.props.dbConfig.dbPort}/billiet/1/product.jpg` }}
          >
            <View style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around'}}>
              <Text style={{flex: 1}}>€{this.props.item.prices.standard}</Text>
            </View>
          </Card>
        </View>
      </TouchableOpacity>
    )
  }
}
