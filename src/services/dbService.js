import PouchDB from 'pouchdb-react-native';
import {Subject} from "rxjs/Rx";

PouchDB.plugin(require('pouchdb-adapter-asyncstorage').default);
PouchDB.plugin(require('pouchdb-upsert'));

export class DbService {
  config;
  remoteDB;
  localDB;
  totalProducts = 0;
  totalProductsSubj = new Subject();
  currentAmountOfProductsSubj = new Subject();

  constructor() {
    this.config = this.getConfig();
    if (!this.remoteDB && !this.localDB) {
      this.remoteDB = new PouchDB(`${this.config.ip}:${this.config.dbPort}/billiet`);
      this.localDB = new PouchDB('billietDb');
    }
    this.updateTotalAmountOfProducts();
    this.updateInitialAmountOfProducts();
  };

  getConfig() {
    return require('../../config/config.json');
  };

  // Total amount in remoteDb
  updateTotalAmountOfProducts() {
    this.remoteDB.allDocs({startkey : 0, limit: 0}).then((items) => {
      this.totalProducts = items.total_rows;
      this.totalProductsSubj.next(this.totalProducts);
    });
  };

  // Initial amount in localDB
  updateInitialAmountOfProducts() {
    this.localDB.allDocs({startkey : 0, limit: 0}).then(items => {
      this.currentAmountOfProductsSubj.next(items.total_rows)
    });
  };

  getCurrentAmountOfProducts() {
    return this.currentAmountOfProductsSubj.asObservable();
  };

  getTotalAmountOfProducts() {
    return this.totalProductsSubj.asObservable();
  };
  
  fetchProducts(start, limit) {
    return this.localDB.allDocs({skip : `${start}`, limit: limit});
  };

  fetchProductById(id) {
    return this.localDB.get(id.toString());
  };

  getAttachment(id, file) {
    return this.localDB.getAttachment(id.toString(), file);
  }

  sync() {
    this.localDB.sync(this.remoteDB, {
      live: true,
      retry: true
    }).on('change', data => {
      console.log('change ', data);
      this.currentAmountOfProductsSubj.next(this.totalProducts - data.change.pending);
    }).on('paused', function (info) {
      console.log('on pause ', this.state);
    }).on('active', function (info) {
      console.log('on active ', info);
    }).on('error', function (err) {
      console.log('on err ', err);
    }).on('complete', function (info) {
      console.log('on complete ', info);
    });
  }
}