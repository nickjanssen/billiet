import {Subject} from "rxjs/Rx";

export class StateService {
  detailSubj = new Subject();

  constructor() {

  };

  setDetail(itemId) {
    this.detailSubj.next(itemId)
  }

  getDetail() {
    return this.detailSubj.asObservable();
  }
}